# JSApp

# Client

Simple client on react, socket.io for connection.

Run "npm run start" for compile script.jsx -> script.js.

Files:

- index.html
- script.jsx 

# Server

Simple node.js server, socket.io for connection.

Files:

- server.js