import React from 'react';
import ReactDOM from 'react-dom';

class Input extends React.Component {
    handleKeyDown(e) {
        if (e.key === 'Enter') {
            console.log(circularStringify(e.target.value));
            this.props.onEnter(e.target.value);
            e.target.value = "";
        }
    }

    render() {
        return <input type="text" onKeyDown={(e) => this.handleKeyDown(e)} />;
    }
}

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isSetName: false,
            isSetRoom: false,
            messages: [],
            users: [],
            disconnect: null
        };

        this.messagesArea = React.createRef();

        socket.on('messages', (messages) => {
            console.log("messages get:" + circularStringify(messages));
            this.setState({ messages: messages });
        });
        socket.on('users', (users) => {
            console.log("users get:" + circularStringify(users));
            this.setState({ users: users });
        });
        socket.on('room_created', (roomId) => {
            urlSetRoomId(roomId);
            this.setState({ isSetRoom: true });
        });
        socket.on('server_error', (msg) => {
            if (this.state.disconnect === null) {
                this.setState({ disconnect: "Disconnect. Server error:" + msg });
            }
            socket.disconnect();
        });
        socket.on('disconnect', () => {
            if (this.state.disconnect === null) {
                this.setState({ disconnect: "Disconnect. Server error: unknown" });
            }
            socket.disconnect();
        });
    }

    componentDidUpdate() {
        // scroll down to last message
        if (this.messagesArea.current != null) {
            this.messagesArea.current.scrollTop = this.messagesArea.current.scrollHeight;
        }
    }

    handleEnterName(name) {
        socket.emit('set_name', name);

        // Get room id from url, if not found create new room
        let roomId = urlGetRoomId();
        if (roomId !== null) {
            socket.emit('join_room', roomId);
            this.setState({ isSetName: true, isSetRoom: true });
        } else {
            this.setState({ isSetName: true });
            socket.emit('create_room');
        }
    }

    handleEnterMessage(msg) {
        socket.emit('send_message', msg);
    }

    render() {
        console.log("render:" + circularStringify(this.state));
        
        if (this.state.disconnect !== null) {
            // Render disconnect status
            return (
                <text>{this.state.disconnect}</text>
            );
        }
        if (!this.state.isSetName) {
            // Render input name
            return [
                <text>Name:</text>,
                <Input onEnter={(name) => this.handleEnterName(name)} />
            ];
        }
        if (!this.state.isSetRoom) {
            // Render connecting screen
            return (
                <text>connecting to room.</text>
            );
        }

        // Render chat
        return [
            <table>
                <tr>
                    <th>
                        <textarea ref={this.messagesArea} rows="10" cols="45" readonly="true" value={this.state.messages.join("\n")} />
                    </th>
                    <th>
                        <textarea rows="10" cols="20" readonly="true" value={"Users:\n" + this.state.users.join("\n")} />
                    </th>
                </tr>
            </table>,
            <text>Msg:</text>,
            <Input onEnter={(msg) => this.handleEnterMessage(msg)} />
        ];
    }
}

var socket = io();

ReactDOM.render(<App />, document.getElementById("root"));

function urlGetRoomId() {
    let res = window.location.pathname.match("\/room\/(.+)\/?$");
    if (res !== null && res.length === 2) {
        return res[1];
    }
    return null;
}

function urlSetRoomId(roomId) {
    window.history.pushState("", null, "/room/" + roomId);
}

function circularStringify(o) {
    var cache = [];
    let res = JSON.stringify(o, function (key, value) {
        if (typeof value === 'object' && value !== null) {
            if (cache.indexOf(value) !== -1) {
                // Duplicate reference
                return "DUP";
            }
            cache.push(value);
        }
        return value;
    });
    cache = null;

    return res;
}
