"use strict";

var http = require("http");
var express = require("express");
var socketIo = require('socket.io');

var app = express();

var serverInstance = http.createServer(app);
var io = socketIo(serverInstance);

var rooms = {}

class Room {
    constructor(id) {
        this.id = id;
        this.messages = [];
        this.users = {};

        console.log("Room:" + this.id + " created");
    }
    addUser(user) {
        console.log("Room:" + this.id + ' add user ' + user.id);
        this.users[user.id] = user;
        this.broadcastUserList();
    }
    delUser(user) {
        console.log("Room:" + this.id + ' delete user ' + user.id);
        delete this.users[user.id];
        if (Object.keys(this.users).length === 0) {
            console.log('Room ' + this.id + " destroy");
            delete rooms[this.id];
        }
        this.broadcastUserList();
    }
    broadcastUserList() {
        let arrayUserNames = [];
        for (let userId in this.users) {
            arrayUserNames.push(this.users[userId].name);
        }
        for (let userId in this.users) {
            this.users[userId].socket.emit('users', arrayUserNames);
        }
    }
    addMessage(user, msg) {
        this.messages.push("[" + getCurrentTimeString() + "] " + user.name + ": " + msg);
        for (let userId in this.users) {
            this.users[userId].socket.emit('messages', this.messages);
        }
    }
}

class User {
    constructor(name, id, socket) {
        this.name = name;
        this.id = id;
        this.socket = socket;
    }
}

class Connection {
    constructor() {
        this.user = null;
        this.room = null;
    }
    isInit() {
        return this.user !== null && this.room !== null;
    }
}

app.use('/script.js', express.static(__dirname + "/script.js"));

app.get("/", function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.get("/room/*", function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

serverInstance.listen(80, function () {
    console.log("listening on *:80");
    io.on("connection", function (socket) {
        console.log('S: a user connected');

        let connection = new Connection();

        socket.on("set_name", function (name) {
            console.log('S: set name');
            if (typeof (name) != "string" ||
                connection.user !== null) {
                socket.emit('server_error', 'incorrect data');
                socket.disconnect(true);
                return;
            }

            connection.user = new User(name, generateId(), socket);
        });

        socket.on("create_room", function () {
            console.log('S: create room');
            if (connection.room !== null || connection.user === null) {
                socket.emit('server_error', 'incorrect data');
                socket.disconnect(true);
                return;
            }

            let room = new Room(generateId());
            room.addUser(connection.user);
            rooms[room.id] = room;
            connection.room = room;

            socket.emit('room_created', room.id);
        });

        socket.on("join_room", function (roomId) {
            console.log('S: join room');
            if (typeof (roomId) != "string" ||
                connection.room !== null ||
                connection.user === null ||
                !(roomId in rooms)) {
                socket.emit('server_error', 'room not exist');
                socket.disconnect(true);
                return;
            }

            connection.room = rooms[roomId];
            rooms[roomId].addUser(connection.user);

            socket.emit('messages', rooms[roomId].messages);

            console.log("join room: ", roomId);
        });

        socket.on("send_message", function (msg) {
            console.log('S: send message');
            if (typeof (msg) != "string" ||
                !connection.isInit()) {
                socket.emit('server_error', 'incorrect data');
                socket.disconnect(true);
                return;
            }

            connection.room.addMessage(connection.user, msg);
        });

        socket.on('disconnect', function () {
            console.log('S: user disconnect');

            if (connection.user !== null && connection.room !== null) {
                connection.room.delUser(connection.user);
            }
        });
    });
});

function generateId() {
    if (generateId.id === undefined) {
        generateId.id = 0;
    }
    return (generateId.id++).toString();
}

function getCurrentTimeString() {
    let ts = Date.now();

    let dateOb = new Date(ts);

    let day = ("0" + dateOb.getDate()).slice(-2);
    let month = ("0" + (dateOb.getMonth() + 1)).slice(-2);
    let year = dateOb.getFullYear();

    let hours = dateOb.getHours();
    let minutes = dateOb.getMinutes();
    let seconds = dateOb.getSeconds();

    return day + "." + month + "." + year + " " + hours + ":" + minutes + ":" + seconds;

}


/*

Client on connect:
1) ("set_name", <string>)
2) ("create_room") or ("join_room", <string>)
3) ("send_message", <string>) infinity times

Server on:
("create_room") do ("room_created", <string>)
("join_room") or disconnect user do room broadcast ("users", [<string>])
("send_message", <string>) do room broadcast ("messages", [<string>])
any error do ("server_error", <string>)

'incorrect data' mean something wrong, or patched client

send a full message list on each message bad for real chat

*/