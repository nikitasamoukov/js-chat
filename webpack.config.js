const path = require("path");

module.exports = {
    entry: path.join(__dirname, '/script.jsx'),
    output: {
        path: __dirname,
        filename: 'script.js',
    },
    module: {
        rules: [
            {
                test: /\.jsx$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env', '@babel/preset-react']
                    }
                }
            }
        ]
    }
}